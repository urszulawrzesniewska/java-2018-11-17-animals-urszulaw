package pl.codementors.zwierzatka;

public class Iguana extends Reptile implements Carnivore {

    public Iguana() {

    }

    public Iguana(String name, int age) {

    }

    @Override
    public void eatmeat() {
        System.out.println(getName() + "The iguana eats meat.");
    }

    public void hiss() {
        System.out.println("hiss");
    }

    @Override
    public void eat() {

    }

    @Override
    public void sound() {
    }
}