package pl.codementors.zwierzatka;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;

public class Main {

    public static void eat(String[] args) { //making all animals eat
        Animal[] animals = new Animal[3];
        animals[0] = new Wolf();
        animals[1] = new Iguana();
        animals[2] = new Parrot();

        for (int i = 0; i < animals.length; i++) {
            animals[i].eat();
            System.out.println("eating");
        }
    }

    public static void sound(String[] args) { //making all animals make sounds
        Animal[] animals = new Animal[3];
        animals[0] = new Wolf();
        animals[1] = new Iguana();
        animals[2] = new Parrot();

        for (int i = 0; i < animals.length; i++) {
            animals[i].sound();
            System.out.println("making a sound");
        }
    }

    public static void howl(String[] args) { //making animal that can howl (Wolf) do so
        int i = 0;
        Wolf[] wolf = new Wolf[i];
        {
            wolf[i].howl();
            System.out.println("howling");
        }
    }

    public static void hiss(String[] args) { //making animal that can hiss (Iguana) do so
        int i = 0;
        Iguana[] iguana = new Iguana[i];
        {
            iguana[i].hiss();
            System.out.println("hissing");
        }
    }

    public static void tweet(String[] args) { //making animal that can tweet (Parrot) do so
        int i = 0;
        Parrot[] parrot = new Parrot[i];
        {
            parrot[i].tweet();
            System.out.println("tweeting");
        }
    }

    public static void eats(String[] args) { //making Carnivores eat meat and Herbivores eat plants
        int i = 0;
        Animal[] animal = new Animal[i];
        if (animal[i] instanceof Carnivore) {
            Carnivore tempCarnivore = (Carnivore) animal[i];
            tempCarnivore.eatmeat();
        }else if (animal[i] instanceof Herbivore) {
            Herbivore tempHerbivore = (Herbivore) animal[i];
            tempHerbivore.eatplants();
        }
    }

    public static void main(String[] args) {

        List<Animal> animals = load("list.txt");
        printall(animals);
    }

    private static void printall(Collection animals) {
        for (Object animal : animals) {
            if (animal instanceof Wolf) {
                System.out.println("Wolf");
                System.out.print(((Wolf) animal).getName() + " ");
                System.out.print(((Wolf) animal).getAge() + " ");
                System.out.print(((Wolf) animal).getFurColour() + "\n");
            } else if (animal instanceof Iguana) {
                System.out.println("Iguana");
                System.out.print(((Iguana) animal).getName() + " ");
                System.out.print(((Iguana) animal).getAge() + " ");
                System.out.print(((Iguana) animal).getScaleColour() + "\n");
            } else if (animal instanceof Parrot) {
                System.out.println("Parrot");
                System.out.print(((Parrot) animal).getName() + " ");
                System.out.print(((Parrot) animal).getAge() + " ");
                System.out.print(((Parrot) animal).getFeatherColour() + "\n");
            }
        }
    }

    static List<Animal> load(String fileName){
            File file = new File("list");
            List<Animal> list = new ArrayList<>();
            try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                int index = Integer.parseInt(br.readLine());
                while (br.ready()) {
                    String species = br.readLine();
                    String name = br.readLine();
                    int age = Integer.parseInt(br.readLine());
                    String colour = br.readLine();

                    if ("Wolf".equals(species)) {
                        Animal tempAnimal = new Wolf();
                        tempAnimal.setName(name);
                        tempAnimal.setAge(age);
                        ((Wolf) tempAnimal).setFurColour(colour);
                        list.add(tempAnimal);
                    } else if ("Iguana".equals(species)) {
                        Animal tempAnimal = new Iguana();
                        tempAnimal.setName(name);
                        tempAnimal.setAge(age);
                        ((Iguana) tempAnimal).setScaleColour(colour);
                        list.add(tempAnimal);
                    } else if ("Parrot".equals(species)) {
                        Animal tempAnimal = new Parrot();
                        tempAnimal.setName(name);
                        tempAnimal.setAge(age);
                        ((Parrot) tempAnimal).setFeatherColour(colour);
                        list.add(tempAnimal);
                    }
                }
                return list;
            } catch (IOException ex) {
                System.err.println(ex);
            }
            return null;
        }
    public static void savebinary(String[] args) throws IOException {
        String targetFile = "animals.dat";
        try (OutputStream output = openFile(targetFile)) {
            output.write(getUtf8Bytes("Animal zoo"));
        }
    }


    private static byte[] getUtf8Bytes(String s) {
        return s.getBytes(StandardCharsets.UTF_8);
    }

    private static BufferedOutputStream openFile(String fileName)
            throws IOException {
        return openFile(fileName, false);
    }

    private static BufferedOutputStream openFile(String fileName, boolean append)
            throws IOException {
        return new BufferedOutputStream(new FileOutputStream(fileName, append));
    }
}
